import logging

from flask import Flask
from flask import jsonify
from prometheus_flask_exporter import PrometheusMetrics

logging.basicConfig(level=logging.INFO)
logging.info("Setting LOGLEVEL to INFO")

api = Flask(__name__)
metrics = PrometheusMetrics(api)

metrics.info("app_info", "App Info: this is a Prometheus metric exporter", version="1.0.0")


@api.route("/")
def first():
    return jsonify({"message": "Hello from the root page"})

@api.route("/hello")
def hello():
    return jsonify(say_hello())

@api.route("/about")
def about():
    return jsonify(call_about())


def say_hello():
    return {"message": "hello from the /hello endpoint"}


def call_about():
    return {"message": "This the the about API"}
